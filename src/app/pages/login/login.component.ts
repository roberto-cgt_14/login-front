import { UsuarioI } from "./../../models/login.interface";
import { ApiService } from "./../../services/api/api.service";
import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  error: boolean = false;
  loginForm = new FormGroup({
    username: new FormControl(""),
    password: new FormControl(""),
  });

  constructor(private api: ApiService, private router: Router) {}

  ngOnInit() {

  }

  loginPost(form: UsuarioI) {
    /* console.log(form); */
    this.api.login(form).subscribe((data) => {
      console.log("data",data)
       localStorage.setItem("user",JSON.stringify(data));

      if (data) {
        this.router.navigate(["/home"]);
      } else {
        this.error = true;
      }
    });
  }
}
