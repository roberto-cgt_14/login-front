import { ResponseI } from "./../../models/response.interface";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HttpClient) {}
  login(form) {
    return this.http.post(
      "https://app-login-back.herokuapp.com/api/login",
      form
    );
  }


 /*  login(form) {
    return this.http.post("/api/login", form);
  } */
}
